[toc]


# Bitbucket

![create new repo](https://bytebucket.org/Jeronen/bitbucket/raw/5d7202bedddb68e8daf35fa31055ddfddfd6af92/img/bitbucket_repo_view.jpg "Repo in Bitbucket cloud")


## Benefits

* [Jira-integration](https://confluence.atlassian.com/jira064/integrating-jira-with-bitbucket-server-720412775.html)

* Trello integration

* Bitbucket creates automatically another repo for [**wiki**](https://bitbucket.org/Jeronen/bitbucket/wiki/Home) inside of your repo that can be filled with documentation that is always on side of the code
* [Bitbucket pipelines](https://www.youtube.com/watch?v=p5KgjeZB8Ww&index=1&list=PLaD4FvsFdarRipSwRNiBKt6CZTrK_ndLn)	
* [LDAP support](https://youtu.be/n731Ys5YTnU?list=PLaD4FvsFdarRipSwRNiBKt6CZTrK_ndLn&t=112)
* [Branching models like gitflow](https://youtu.be/n731Ys5YTnU?list=PLaD4FvsFdarRipSwRNiBKt6CZTrK_ndLn&t=590)
* [Advanced branch nagivation](https://youtu.be/n731Ys5YTnU?list=PLaD4FvsFdarRipSwRNiBKt6CZTrK_ndLn&t=764)
  * Compare branches inside of bitbucket
* [Easy to use code review tools](https://youtu.be/n731Ys5YTnU?list=PLaD4FvsFdarRipSwRNiBKt6CZTrK_ndLn&t=814) 
* [Sourcetree support](https://confluence.atlassian.com/bitbucketserver/set-up-sourcetree-to-work-with-bitbucket-server-776798217.html) for those who like it
* [Addons](https://marketplace.atlassian.com/search?product=bitbucket)
* Well documented, internet is basically full of bitbucket docs and videos how to setup and use things

## Differences between bitbucket server and cloud

* Issue tracking and wiki - Bitbucket Cloud offers basic wiki support and issue tracking. With the availability of Confluence for documentation, and JIRA Software for integrated software development planning and tracking, it is not our intention to build the same into Bitbucket Server, however we will continue to improve upon the integration with these products.
* Snippets - There are no immediate plans to add support for snippets to Bitbucket Server. A 3rd-party addon is available for those wishing to manage code snippets in Bitbucket Server.

## Links
[This document hosted in Bitbucket](https://bitbucket.org/Jeronen/bitbucket/wiki/Home)

[Bitbucket.org](https://bitbucket.org/product)

[Video demostration about the workflow (elevator music included)](https://www.youtube.com/watch?v=OMLh-5O6Ub8&list=PLaD4FvsFdarRipSwRNiBKt6CZTrK_ndLn&index=5)

[Integrations](https://bitbucket.org/product/integrations)

[Pricing](https://bitbucket.org/product/pricing?tab=host-in-the-cloud)
